<?php
function auth($login, $passwd)
{
    if ($login == NULL || $passwd == NULL)
    {
        return (FALSE);
    }
    $array_of_users = unserialize(file_get_contents("../private/passwd"));
    $user_num = find_login($array_of_users, $login);
    if (!$user_num)
    {
        return (FALSE);
    }
    if ($array_of_users[$user_num - 1]['passwd'] == hash('whirlpool', $passwd))
    {
        return (TRUE);
    }
    else
    {
        return (FALSE);
    }
}

function    find_login($array, $login)
{
    $i = 0;
    foreach ($array as $elem)
    {
        $i++;
        if ($elem['login'] == $login)
        {
            return ($i);
        }
    }
    return (0);
}
?>