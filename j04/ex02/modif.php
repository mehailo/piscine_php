<?php
    if ($_POST['submit'] == 'OK')
    {
        if ($_POST['newpw'] == NULL || $_POST['oldpw'] == NULL || $_POST['login'] == NULL || !file_exists("../private/passwd"))
        {
            echo "ERROR\n";
            return ;
        }
        $array_of_users = unserialize(file_get_contents("../private/passwd"));
        $user_num = find_login($array_of_users, $_POST['login']);
        if (!$array_of_users || !$user_num)
        {
            echo "ERROR\n";
            return   ;
        }
        $login = $_POST['login'];
        if ($array_of_users[$user_num - 1]["passwd"] != hash('whirlpool', $_POST['oldpw']) || hash('whirlpool', $_POST['oldpw']) == hash('whirlpool', $_POST['newpw']))
        {
            echo "ERROR\n";
            return   ;
        }
        $array_of_users[$user_num - 1]["passwd"] = hash('whirlpool', $_POST['newpw']);
        file_put_contents("../private/passwd", serialize($array_of_users));
        echo "OK\n";
        return ;
    }

function    find_login($array, $login)
{
    $i = 0;
    foreach ($array as $elem)
    {
        $i++;
        if ($elem['login'] == $login)
        {
            return ($i);
        }
    }
    return (0);
}
?>